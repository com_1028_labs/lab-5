package com.com1028.travelagency;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * 
 * Tests that the CurencyConverter class is returning the correct exchange amount
 * Annotations are used in these tests
 * 
 * @author Mariam Cirovic
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class CurrencyConverterTest {

	// @InjectMocks annotation is used to create and inject the mock object
	@InjectMocks
	CurrencyConverter converter = new CurrencyConverter();

	// @Mock annotation is used to create the mock object to be injected
	@Mock
	CurrencyService service;

	@Test
	public void testConvertCurrency() {
		// Method chain to return 1.12 as the exchange rate between pound and euro
		when(service.getExchangeRate("pound", "euro")).thenReturn(1.12);
		assertEquals(1120.0, converter.convertCurrency(1000.0, "pound", "euro"), 0);
		verify(service).getExchangeRate("pound", "euro");
	}

	@Test(expected = RuntimeException.class)
	public void testConvertCurrencyException() {

		doThrow(new RuntimeException("ExchangeRate not implemented")).when(service.getExchangeRate("pound", "euro"));
		assertEquals(1120.0, converter.convertCurrency(1000.0, "pound", "euro"), 0);
		verify(service).getExchangeRate("pound", "euro");
	}
}