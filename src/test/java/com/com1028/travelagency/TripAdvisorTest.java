package com.com1028.travelagency;

import static org.junit.Assert.*;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * 
 * Tests that the TripAdvisor class is retrieving an excursion list of the required type
 * as well as retrieving the correct weather information
 * Annotations are used in these tests
 * 
 * @author Mariam Cirovic
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class TripAdvisorTest {

	@InjectMocks
	TripAdvisor tripAdvisor = new TripAdvisor();
	
	// @Mock annotation is used to create the mock object to be injected
	@Mock
	ExcursionService excursionService;
	
	@Mock
	WeatherService weatherService;
	
	@Test
	public void testExcursionList() {
		String city = "Chania";
		List<String> excursions = new ArrayList<String>();
		excursions.add("Samaria Gorge");
		excursions.add("Santorini Island");
		excursions.add("Elafonisi Beach");
		excursions.add("Gavdos Island");
		excursions.add("Preveli Beach");
		
		String type = "beach";
		List<String> beachExcursions = new ArrayList<String>();
		beachExcursions.add("Elafonisi Beach");
		beachExcursions.add("Preveli Beach");
		
		// Method chain to return the above lsit of excursions when a city "Chania" is specified
		when(excursionService.excursionList(city)).thenReturn(excursions);
		assertEquals(beachExcursions, tripAdvisor.getExcursionList(city, type));
		verify(excursionService).excursionList(city);
	}
	
	
	@Test
	public void testWeatherInfo() {
		String city = "Chania";
		Integer month = 9;
		InOrder inOrder = inOrder(weatherService);
		
		when(weatherService.predictedAverageTemperature(city, month)).thenReturn(27);
		when(weatherService.predictedRainProbability(city, month)).thenReturn(0.15);
		assertEquals("Warm and Dry", tripAdvisor.weatherInfo(city, month));
		
		inOrder.verify(weatherService).predictedAverageTemperature(city, month);
	    inOrder.verify(weatherService).predictedRainProbability(city, month);
	}
}
