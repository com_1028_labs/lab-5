package com.com1028.travelagency;

import static org.junit.Assert.*;



import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 * Tests that the TripManager class is calculating the discount correctly
 * Also a Spy on an itinerary object is created and tested
 * Static methods of Mockito are used
 * 
 * @author Mariam Cirovic
 * 
 */
public class TripManagerTest {

	TripManager manager;
	
	// Mock the Trip Database class
	TripDatabase db = mock(TripDatabase.class);
	
	@Before
	public void setUp() {
		manager = new TripManager(db);
	}
	
	@Test
	public void testApplyDiscount() {
		
		// Method chain (stub) to state that the customerID c11 should return the value 2000.00
		when(db.totalTripCost("c11")).thenReturn(2000.0);
		assertEquals(1600.0, manager.applyDiscount(20, "c11"), 0);
		verify(db).totalTripCost("c11");	
		
		when(db.totalTripCost("c20")).thenReturn(5000.0);
		assertEquals(4000.0, manager.applyDiscount(20, "c20"), 0);
		//assertEquals(4000.0, manager.applyDiscount(20, "c20"), 0);
		verify(db).totalTripCost("c20");
		
		//check if method is called once
	     verify(db, times(1)).totalTripCost("c20");
	      
	     //verify that method was never called on a mock
	     verify(db, never()).cityItinerary("c11");
	}

	@Test
	public void testItinerarySpy() {
		List<String> itinerary = new ArrayList<String>();
		
		itinerary.add("London");
		itinerary.add("Paris");
		itinerary.add("Rome");		
		
		// Create a spy to spy on the real instance itinerary
		List<String> itinerarySpy = spy(itinerary);
		
		assertEquals("London", itinerarySpy.get(0));
		
		// This is a stub - it states that return "Madrid" in position 1 instead of "Paris"
		// You can use a spy to do a partial mock
		doReturn("Madrid").when(itinerarySpy).get(1);
		assertEquals("Madrid", itinerarySpy.get(1));
		
		assertEquals("Rome", itinerarySpy.get(2));
		
		System.out.println("Number of cities: " + itinerarySpy.size());
		
		// Add another city to the spy object
		itinerarySpy.add("Athens");
		
		System.out.println("Number of cities (spy): " + itinerarySpy.size());
		// The size of itinerary is unchanged - the object was only added to the spy
		// The spy has all the behaviour of the real object so it is mostly used to 
		// to explore legacy code that does not come with a clearly defined interface
		System.out.println("Number of cities (original): " + itinerary.size());
				
	}
}
