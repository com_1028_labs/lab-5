package com.com1028.travelagency;

/**
 * 
 * Interface that defines the behaviour of a Currency Service
 * 
 * @author Mariam Cirovic
 * 
 */
public interface CurrencyService {
	// This method returns the exchange rate given the names of two currencies 
	public double getExchangeRate(String currency1, String currency2);
}
