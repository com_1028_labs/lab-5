package com.com1028.travelagency;

/**
 * 
 * Defines the properties and behaviour for a Currency Converter class
 * 
 * @author Mariam Cirovic
 * 
 */
public class CurrencyConverter {
	CurrencyService service;
	
	public CurrencyConverter() {
		this.service = null;
	}
	
	// The CurrencyService is a dependency passed via a setter method
	public void setCurrencyService(CurrencyService service) {
		this.service = service;
	}
	
	/**
	 * Retrieve the new currency amount given a amount in currency 1 to be converted to currency 2
	 *
	 * @return currency amount
	 */	
	public double convertCurrency(double amount, String currency1, String currency2) {
		double newCurrencyAmount;
		
		// Call the currency service to get the exchange rate between currencies 1 & 2
		double exchangeRate = service.getExchangeRate(currency1, currency2);
		newCurrencyAmount = amount * exchangeRate;
		return newCurrencyAmount;
		
	}
}
