package com.com1028.travelagency;

import java.util.List;
/**
 * 
 * Defines the properties and behaviour for TripManager
 * to apply a discount to the total trip cost and get the itinerary
 * 
 * @author Mariam Cirovic
 * 
 */

public class TripManager {
	
	private TripDatabase tripDB;
	
	/**
	 * Constructor sets the trip database
	 * TripDatabase dependency is passed via the constructor
	 * @param tripDB 
	 * 			The database with details on Customers' Trips
	 */

	public TripManager(TripDatabase tripDB) {
		this.tripDB = tripDB;
	}
	
	/**
	 * Retrieve the cost of a customers trip after applying a discount
	 * 
	 * @return discounted cost
	 */	
	public double applyDiscount(int percentageDiscount, String CustomerID) {
		double cost = tripDB.totalTripCost(CustomerID);
		return cost - (cost *percentageDiscount/100);
		//return 1600.00;
	}
	
	/**
	 * Retrieve the itinerary of a customer's trip given the customer id
	 * 
	 * @return a list of places a traveller will visit in order
	 */		
	public List<String> getItinerary(String CustomerID){
		return tripDB.cityItinerary(CustomerID);
	}

}
