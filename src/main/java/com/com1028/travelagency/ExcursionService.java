package com.com1028.travelagency;

import java.util.List;

/**
 * 
 * Interface that defines the behaviour of an Excursion Service
 * 
 * @author Mariam Cirovic
 * 
 */
public interface ExcursionService {
	// This method returns the list of excursions that are provided for a given city
	public List<String> excursionList(String city);
}
