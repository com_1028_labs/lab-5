package com.com1028.travelagency;

import java.util.List;
/**
 * 
 * Interface that defines the behaviour of a Trip Database Service
 * 
 * @author Mariam Cirovic
 * 
 */
public interface TripDatabase {
	// This method returns  the total cost of a trip for a given customer ID
	public double totalTripCost(String CustomerID);
	// This method returns  the list of cities to be visited in order by the customer given the ID
	public List<String> cityItinerary(String CustomerID);
}
