package com.com1028.travelagency;

/**
 * 
 * Interface that defines the behaviour of a Weather Service
 * 
 * @author Mariam Cirovic
 * 
 */
public interface WeatherService {
	// This method returns the predicted average temperature for a given city in a given month
	// The month is expressed as a digit e.g. September = 9
	public Integer predictedAverageTemperature(String city, Integer month);
	
	// This method returns the predicted rain probability for a given city in a given month
	// The month is expressed as a digit e.g. September = 9	
	public Double predictedRainProbability(String city, Integer month);
}
