package com.com1028.travelagency;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Defines the properties and behaviour for a Trip Advisor class
 * It provides weather info and available excursions for a given city
 * 
 * @author Mariam Cirovic
 * 
 */
public class TripAdvisor {
	ExcursionService excursionService;
	WeatherService weatherService;
	
	public TripAdvisor() {
		excursionService = null;
		weatherService = null;
	}
	
	// The excursionService is a dependency passed via a setter method
	public void setExcursionService(ExcursionService excursionService) {
		this.excursionService = excursionService;
	}
	
	public void setWeatherService(WeatherService weatherService) {
		this.weatherService = weatherService;
	}
	
	/**
	 * Retrieve the excursions of a given type offered in a specified city 
	 * e.g. beach, island, historic
	 * 
	 * @return list of excursions
	 */		
	public List<String> getExcursionList(String city, String type){
		List<String> allExcursions = new ArrayList<String>();
		
		// Call the excursion service to get all the excursions listed for a particular city
		allExcursions = excursionService.excursionList(city);
		
		List<String> chosenExcursions = new ArrayList<String>();
		for(String excursion: allExcursions) {
			//Checking to see if the excursion is of the type specifies e.g. beach
			if (excursion.toLowerCase().contains(type.toLowerCase())){
				chosenExcursions.add(excursion);
			}
		}
		return chosenExcursions;
	}

	/**
	 * Retrieve the weather information for specified city in a specific month
	 * 
	 * @return the weather info as a string
	 */	
	public String weatherInfo(String city, Integer month) {
		Integer temp = weatherService.predictedAverageTemperature(city, month);
		Double rainprob = weatherService.predictedRainProbability(city, month);
		
		String weather = null;
		
		if (temp >= 21 && rainprob >= 0.5) {
			weather = "Warm and Rainy";
		} else if (temp >= 21 && rainprob < 0.5) {
			weather = "Warm and Dry";
		} else if (temp < 21 && rainprob < 0.5){
			weather = "Cold and Dry";
		} else {
			weather = "Cold and Rainy";
		}
		
		return weather;
	}	
}

